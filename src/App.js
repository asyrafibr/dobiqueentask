import logo from "./logo.svg";
import "./App.css";
import { Button } from "react-bootstrap";
import moment from "moment";
import axios from "axios";

let api = axios
  .create({ baseURL: `https://api.spaceflightnewsapi.net/v3/v3/articles` })
  .get("/")
  .then((res) => {
    console.log("data Fetch", res.data);
  })
  .catch((error) => {
    console.log("DATA Fetch Error:", error.response.data);
  });
let paragrapText =
  " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed doeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi aliquip ex ea commodo consequat. Duis aute irure dolor inreprehenderit in voluptate velit esse cillum dolore eu fugiat nullapariatur. Excepteur sint occaecat cupidatat non proident, sunt inculpa qui officia deserunt mollit anim id est laborum";

let paragraphHeader = "Lorem ipsum dolor";

function App() {
  console.log("data Fetch", api);
  return (
    <div>
      <div style={{ padding: "20px", backgroundColor: "#ffe200" }}>
        <div style={{      flexDirection: "row",
            display: "flex",
            justifyContent: "space-between",}}>
          <div></div>
          <img
            src={require("./pic.png")}
            style={{ display: "flex", alignSelf: "center" ,   resizeMode: 'contain'
           }}
          />
          <div></div>
        </div>

        <h1>{paragraphHeader}</h1>

        <p>{paragrapText}</p>
        <div
          style={{
            flexDirection: "row",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <div
            style={{
              backgroundColor: "#e00069",
              width: "30%",
              padding: "20px",
              borderColor: "#0c336f",
              borderStyle: "solid",
            }}
          >
            <h3>{moment(new Date()).format("DD/MM/YYYY")}</h3>

            <b>
              <h2> {paragraphHeader}</h2>
            </b>
            <p style={{backgroundColor: "#a2ceef", padding:'3px',paddingLeft:"10px", paddingBottom:'50px'}}>{paragrapText}</p>

            <Button
              style={{
                float: "left",
                marginTop: "5%",
                padding: "20px",
                borderStyle: "solid",
                backgroundColor: "white",
              }}
            >
              Read More
            </Button>
          </div>

          <div
            style={{
              backgroundColor: "#e00069",
              width: "30%",
              padding: "20px",
              borderStyle: "solid",
              borderColor: "#0c336f",
            }}
          >
            <h3>{moment(new Date()).format("DD/MM/YYYY")}</h3>

            <b>
              <h2>{paragraphHeader}</h2>
            </b>
            <p style={{backgroundColor: "#a2ceef", padding:'3px',paddingLeft:"10px", paddingBottom:'50px'}}>{paragrapText}</p>
            <Button
              style={{
                float: "left",
                marginTop: "5%",
                padding: "20px",
                borderStyle: "solid",
                backgroundColor: "white",
              }}
            >
              Read More
            </Button>
          </div>
          <div
            style={{
              backgroundColor: "#e00069",
              width: "30%",
              padding: "20px",
              borderStyle: "solid",
              borderColor: "#0c336f",
            }}
          >
            <h3>{moment(new Date()).format("DD/MM/YYYY")}</h3>

            <b>
              <h2> {paragraphHeader}</h2>
            </b>
            <p style={{backgroundColor: "#a2ceef", padding:'3px',paddingLeft:"10px", paddingBottom:'50px'}}>{paragrapText}</p>
            <Button
              style={{
                float: "left",
                marginTop: "5%",
                padding: "20px",
                borderStyle: "solid",
                backgroundColor: "white",
              }}
            >
              Read More
            </Button>
          </div>
        </div>

        <div
          style={{
            flexDirection: "row",
            display: "flex",
            justifyContent: "space-between",
            marginTop: "20px",
          }}
        >
          <div
            style={{
              backgroundColor: "#e00069",
              width: "30%",
              padding: "20px",
              borderStyle: "solid",
              borderColor: "#0c336f",
            }}
          >
            <h3>{moment(new Date()).format("DD/MM/YYYY")}</h3>

            <b>
              <h2> {paragraphHeader}</h2>
            </b>
            <p style={{backgroundColor: "#a2ceef", padding:'3px',paddingLeft:"10px", paddingBottom:'50px'}}>{paragrapText}</p>
            <Button
              style={{
                float: "left",
                marginTop: "5%",
                padding: "20px",
                borderStyle: "solid",
                backgroundColor: "white",
              }}
            >
              Read More
            </Button>
          </div>

          <div
            style={{
              backgroundColor: "#e00069",
              width: "30%",
              padding: "20px",
              borderStyle: "solid",
              borderColor: "#0c336f",
            }}
          >
            <h3>{moment(new Date()).format("DD/MM/YYYY")}</h3>

            <b>
              <h2>{paragraphHeader}</h2>
            </b>
            <p style={{backgroundColor: "#a2ceef", padding:'3px',paddingLeft:"10px", paddingBottom:'50px'}}>{paragrapText}</p>
            <Button
              style={{
                float: "left",
                marginTop: "5%",
                padding: "20px",
                borderStyle: "solid",
                backgroundColor: "white",
              }}
            >
              Read More
            </Button>
          </div>
          <div
            style={{
              width: "30%",
              padding: "20px",
              borderStyle: "solid",
              backgroundColor: "#e00069",
              borderColor: "#0c336f",
            }}
          >
            <h3>{moment(new Date()).format("DD/MM/YYYY")}</h3>
            <b>
              <h2> {paragraphHeader}</h2>
            </b>
            <p style={{backgroundColor: "#a2ceef", padding:'3px',paddingLeft:"10px", paddingBottom:'50px'}}>{paragrapText}</p>
            <Button
              style={{
                float: "left",
                marginTop: "5%",
                padding: "20px",
                borderStyle: "solid",
                backgroundColor: "white",
              }}
            >
              Read More
            </Button>
          </div>
        </div>
        <div>
          <Button
            style={{
              marginTop: "2%",
              padding: "20px",
              marginLeft: "50%",
              borderStyle: "solid",
              backgroundColor: "white",
            }}
          >
            Load More
          </Button>
        </div>
      </div>
    </div>
  );
}

export default App;
